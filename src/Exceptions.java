public class Exceptions {
	
	public class SubExceptions extends Exceptions
	{
		public SubExceptions() {
			try{
				ThrowCustomException();
			}
			catch(UserDefinedException ex)
			{
				System.out.println(ex.getMessage());
			}
		}
		// Overriding method can throw only exceptions defined in the base class or nothing
		void ThrowCustomException() throws UserDefinedException
		{
			throw new UserDefinedException("Overrided ThrowCustomException() exception");
		}
	}
	
	public Exceptions() {

		System.out.println("_______________________\nExceptionLeaks example:");
		ExceptionLeaks();
		System.out.println("_______________________\n");

		System.out.println("________________________________\nException specification exapmle:");
		try{
			ThrowCustomException();
		}
		catch(UserDefinedException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			System.out.println("________________________________\n");
		}
		
		// new SubExceptions(); // will cause a recursion :)		
		
	}
	
	void ExceptionLeaks()
	{
		try{
			try{
				throw new Exception("Inner try excpetion");
			}
			//@SuppressWarnings
			finally{
				throw new Exception("Inner finally exception"); // this will replace try's exception
				// return true; // Using �return� inside the finally block will silence any thrown exception.
			}
			
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
		}
	}
	
	void ThrowCustomException() throws UserDefinedException
	{
		throw new UserDefinedException("ThrowCustomException() exception");
	}
}


class UserDefinedException extends Exception
{
	public UserDefinedException() {
		// TODO Auto-generated constructor stub
	}
	
	public UserDefinedException(String message) {
		super(message);
	}
}
