package package1;

public class Classes {
	public static long term = 100500;
}

class Class1{	// default access - accessible for entire package only
	static long term = 100500; 			// default access - accessible for entire package only
}

class Class2{
	public static long term = 100500;	
	{
		System.out.println(Classes.term);
	}
}