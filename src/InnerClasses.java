public class InnerClasses {
	
	private int number;
	
	class InnerClass{
		class InnerInnerClass{
			
		}
		
	}
	
	public InnerClasses() {
		new Object() {
			{
				System.out.println("Non-static initialization of anonimous class which extends Parent class");
				number++;
			}
		};
		LocalClass("constant value",100500);
		
		OuterClass oc = new OuterClass();
    	//OuterClass.InnerClass ic = oc.new InnerClass();
    	InheritedInnerClass iic = new InheritedInnerClass(oc);
    	
    	ClassIHasInner hasInnerimplemented = new ClassIHasInner();
	}
	
	public void LocalClass(final String finalValue, int nonFinal){
		class Local {
			{
				number = 10; 				// has access to all the members of the enclosing class.
				String value = finalValue;  // does have access to the final variables in the current code block
				//nonFinal = 0; 			// have no access to non-final variables of the current code block
				System.out.println("Non-static initialization of local class");					
			}
		} 
		new Local();
	}
}

class Parent
{
	public Parent(int num) {
		// TODO Auto-generated method stub
		System.out.println("Parent ctor says " + num);
	}
}


class OuterClass{
    class InnerClass{
        public InnerClass() {
            System.out.println("InnerClassCtor");
        }
    }

    public OuterClass() {
        System.out.println("Outer class ctor.");
    }
}

class InheritedInnerClass extends OuterClass.InnerClass
{
    // as inner class always contains a ref to super class compiler
    // requires it to be created explicitly using a call to its ctor 
    public InheritedInnerClass(OuterClass oc) {
       oc.super();
       System.out.println("InheritedInnerClass ctor.");
    } 
}

interface IHasInner{
	static class Inner{	// public static by default
		public int innerInt = 100;
		public void Speak(){}
	}	
}


class ClassIHasInner implements IHasInner{
	int intField;
	class Nested{
		{intField = 10;}
	}
	class StaticNested{
		// static{intField = 10;} // error
	}
	
	{
		IHasInner.Inner inr = new Inner();
		inr.innerInt = 200;
		System.out.println(inr.innerInt);
	}
	
	
}