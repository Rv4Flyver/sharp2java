
public class Enums {
	public static enum  Days{
		MONDAY, THUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, 
		SUNDAY {public int getDayValue(){
					return 7;
				} 
		}, 
		SOMEDAY(100500);
		public int value;
		private Days(){
			value = -1;
		}
		private Days(int i){
			value = i;
		}
		public int getDayValue(){
			return value;
		}
	}
}
