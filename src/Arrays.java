public class Arrays {
	public Arrays() {
		
	}
	
	public void GetArray()
	{
		System.out.println("GetArray()");
	}
	
	/**
	 * Short version of defining an array of arguments
	 * @param args
	 */
	public void GetArray(Object... args)
	{
		System.out.println("Object... args");		
	}
	
	public void GetArray(String... args)
	{
		System.out.println("String... args");		
	}
	
}
