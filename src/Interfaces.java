import java.util.*;

interface LolFace
{
	interface SadFace
	{
		int NUM = 0;		
	}
	Date date = new Date();
	int NUM = new Random(date.getSeconds()).nextInt(date.getSeconds());
}

public class Interfaces implements LolFace  {
	
	public Interfaces() {
		System.out.println("Interfaces ctor says: " + (char)NUM + " [code: " + NUM +  " ]");
		new Interfaces2();
	}
}

class Interfaces2 implements LolFace.SadFace  {
	
	public Interfaces2() {
		System.out.println("Interfaces2 ctor says: " + (char)NUM + " [code: " + NUM +  " ]");
		
	}
	
}
