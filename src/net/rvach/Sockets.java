package net.rvach;
import java.io.*;
import java.net.*;

public class Sockets {
	{
		Thread server = new Thread(new ServerRunnable());
		Thread client1 = new Thread(new ClientRunnable());		
		Thread client2 = new Thread(new ClientRunnable());	
		Thread client3 = new Thread(new ClientRunnable());	
		
		server.start();
		client1.start();		
		try {
			Thread.sleep(1000);
			client2.start();		
			Thread.sleep(1000);
			client3.start();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
}

class ServerRunnable implements Runnable{
	public void run() {
		Socket s = null;
		ServerSocket server;
		try {
			server = new ServerSocket(8030);
			int clientsConnected = 0;
			while(clientsConnected<3)
			{
				s = server.accept(); 
				clientsConnected++;
				System.out.println("| server: " + s.getInetAddress().getHostName() + " connected");
				PrintStream pStream = new PrintStream(s.getOutputStream());
				pStream.println("Hello Client �"+clientsConnected);
				pStream.flush();
			}
			s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

class ClientRunnable implements Runnable{

	@Override
	public void run() {
		Socket socket = null;
		try {
			socket = new Socket("localhost",8030);
			InputStreamReader iSTreamReader = new InputStreamReader(socket.getInputStream());
			BufferedReader bReader = new BufferedReader(iSTreamReader);
			
			System.out.println("| Client has received message from server which says: '"+bReader.readLine() + "'");
			
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
