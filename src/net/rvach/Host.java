package net.rvach;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Host {
	static Host getHost;
	InetAddress local;
	
	
	public static Host Get() throws UnknownHostException{
		getHost = getHost==null ? new Host() : getHost;
		return getHost;
	}
	
	private Host() throws UnknownHostException{
		local = InetAddress.getLocalHost();
	}
	
	public InetAddress GetLocalhost(){
		return local;
	}
	
	public InetAddress GetHost(String hostName){
		try {
			return InetAddress.getByName(hostName);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return local;
	}
}
