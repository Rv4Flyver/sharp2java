import java.util.*;
public class Generics {
	public Generics() {
		// bounds
		new Derived(new SubType());	// an example of bounds use and polimorfism
		
		//	wildcards and their bounds
		String[] strArr = new String[]{"one","two","three"};
		List<?> stringList = java.util.Arrays.asList(strArr); // in particular case is the same as
															  // List<String> stringList = (List<String>)java.util.Arrays.asList(strArr);
		new SimpleWildcard(stringList);
		
		Integer[] intArr = new Integer[]{new Integer(1),new Integer(2),new Integer(3)};
		List<Integer> intList = (List<Integer>)java.util.Arrays.asList(intArr);
		new SimpleWildcard(intList);
		
	}
}

//an example of bounds use and polimorfism
class BaseType{
	public void Method1(){
		System.out.println("Method1 of BaseType is invoked");
	}
}

class SubType extends BaseType{
	public void Method1(){
		System.out.println("Method1 of SubType is invoked");
	}
}

class Base<T extends BaseType>{
	
}

//class Derived<T> extends Base<T> // generic types of derived classes should have the same bounds as the base class ones
class Derived<T extends BaseType> extends Base<T>
{
	public Derived(T baseObj) {
		baseObj.Method1();
	}
}
// ---------------------------------------

//              wildcards and their bounds

class SimpleWildcard{
	public SimpleWildcard(List<?> list) {
		//list.add("4");	
		for(Object o : list) System.out.println(o);
	}
}