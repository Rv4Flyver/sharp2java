public class ObjectLifeCycle {
	
	static int staticInt;
	int number;
	
	/** A construction which is an analog of C#'s static constructor*/
	static
	{
		staticInt = 100500;
		System.out.println("Static initialization!");
	}
	
	/** 
	 * Also Java had a non-static instance initialization construction 
	 * This syntax is necessary to support the initialization of anonymous inner classes (see the Inner
	 * Classes chapter), but it also allows you to guarantee that certain operations occur regardless
	 * of which explicit constructor is called. From the output, you can see that the instance
	 * initialization clause is executed before either one of the constructors
	*/
	{
		number = 100500;
		System.out.println("Non-Static initialization!");
	}
	public ObjectLifeCycle() {
		System.out.println(staticInt);
	}
	

}
