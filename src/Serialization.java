import java.io.*;
import java.util.jar.Attributes.Name;

public class Serialization {
	String path = "output/record_15";
	
	public Serialization() {
		SerializableRecord record = new SerializableRecord(15, "Rororor", 17);
		
		try{
			Serialize(path, record);
			
			SerializableRecord record2 = (SerializableRecord) Deserialize(path);
			record2.ShowEntity();
		}
		catch(IOException ex){
			System.out.println("IOException: " + ex.getMessage());			
		}
		catch(ClassNotFoundException ex){
			
		}
		
		
	}
	
	public void Serialize(String path, Serializable record) throws IOException{
		FileOutputStream stream = new FileOutputStream(path);
		ObjectOutputStream outputWriter = new ObjectOutputStream(stream);
		outputWriter.writeObject(record);
		outputWriter.close();
		stream.close();
	}
	
	public Serializable Deserialize(String path) throws IOException, ClassNotFoundException{
		FileInputStream stream = new FileInputStream(path);
		ObjectInputStream inputReader = new ObjectInputStream(stream);
		Serializable result =  (Serializable) inputReader.readObject();
		inputReader.close();
		stream.close();
		return result;
	}
}

class SerializableRecordSuper{
	protected char customSymbol = '@';
}

class SerializableRecord extends SerializableRecordSuper implements Serializable{
	public int id;
	public String name;
	transient int age;
	
	public SerializableRecord(int id, String name, int age) {
		this.id = id;
		this.name = name;
		this.age = age;
	}
	
	public void ShowEntity(){
		String line;
		line = " | "+id+" | ";

		System.out.println(line);

		for(int i =0; i < line.length(); i++){
			System.out.print("-");			
		}
		System.out.println();
		
		char[] chars = name.toCharArray();
		System.out.print("|");
		for(int i =0; i < line.length()-2; i++){
			if( i == Math.floor(line.length()/2))
				System.out.print(age);
			else
			System.out.print(" ");			
		}	
		System.out.println("|");
		
		// base class constructor is running while SerializableRecord is desearialization
		for(int i =0; i < line.length(); i++){
			System.out.print(customSymbol);			
		}	
		
		
	}
}
