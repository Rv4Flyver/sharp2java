import java.io.*;
import java.net.UnknownHostException;

import net.rvach.*;

public class Sharp2Java{
	protected Sharp2Java(){;}
	
	public static void main(String[] args){
		//System.out.printf("[%2$10.5d Asides %1$t sadist]", 1, 2.5543534543);
		
		System.out.println("_O_B_J_E_C_T__L_I_F_E__C_Y_C_L_E__E_X_A_M_P_L_E_");
		new ObjectLifeCycle();
		System.out.println("");
		
		System.out.println("_A_R_R_A_Y_S__E_X_A_M_P_L_E_");	
		Arrays arrs = new Arrays();
		// passing no arguments when array is expected is okay, but it can lead for the following: 
		arrs.GetArray();	// The method GetArray(Object[]) is ambiguous for the type Arrays if no method without args is defined
		arrs.GetArray(new String[]{});
		System.out.println("");
		
		System.out.println("_I_N_T_E_R_F_A_C_E__E_X_A_M_P_L_E_");	
		new Interfaces(); 
		System.out.println("");
		
		System.out.println("__I_N_N_E_R__C_L_A_S_S_E_S__E_X_A_M_P_L_E_");	
		new InnerClasses();
		System.out.println("");
		
		System.out.println("_E_X_C_E_P_T_I_O_N_S__E_X_A_M_P_L_E_");	
		Exceptions exs = new Exceptions();
		exs.new SubExceptions();
		System.out.println("");
		
		System.out.println("_R_E_G_E_X_P_S__E_X_A_M_P_L_E_");
		new Regexps();
		System.out.println("");
		
		System.out.println("_R_T_T_I__E_X_C_E_P_T_P_T_I_O_N_");
		new RTTI();
		System.out.println();
		
		System.out.println("_G_E_N_E_R_I_C_S_");
		new Generics();
		System.out.println("");
		
		System.out.println("S_E_R_I_A_L_I_Z_A_T_I_O_N__E_X_A_M_P_L_E_");
		new Serialization();
		System.out.println("");
		
		
		System.out.println("T_H_R_E_A_D_I_N_G__E_X_A_M_P_L_E_");
		new Threading();
		System.out.println("");		

		System.out.println("A_C_C_E_S_S_I_B_I_L_I_T_Y_");
		new Accessibility();
		System.out.println("");			
		
		
		System.out.println("_E_N_U_M_S__C_Y_C_L_E__E_X_A_M_P_L_E_");
		Enums.Days d = Enums.Days.SUNDAY;
		System.out.println(d.getDayValue());
		System.out.println("");
		
		System.out.println("_N_E_T_W_O_R_K_");
		Host _Host = null;
		try {
			_Host = Host.Get();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("localhost: " + _Host.GetLocalhost());
		try {
			System.out.println("google.by: "+ _Host.GetHost("www.google.by") + "| Is reachable: " + _Host.GetHost("www.google.by").isReachable(1000));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("");
		
		System.out.println("_S_O_C_K_E_T_S_");
		new Sockets();
		


	}
}

class A 
{ 
    A( ) { } 
} 

class B extends A 
{ }
