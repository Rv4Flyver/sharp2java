import java.util.regex.*;
public class Regexps {

	 static public final String POEM =
			 "Twas brillig, and the slithy toves\n" +
			 "Did gyre and gimble in the wabe.\n" +
			 "All mimsy were the borogoves,\n" +
			 "And the mome raths outgrabe.\n\n" +
			 "Beware the Jabberwock, my son,\n" +
			 "The jaws that bite, the claws that catch.\n" +
			 "Beware the Jubjub bird, and shun\n" +
			 "The frumious Bandersnatch.\n" + 
			 "3ND 0F L1N3";
	
	public Regexps() {
		 Groups();
		
		 Replacements();
	}
	
	 void Groups() {
		 Matcher m = Pattern.compile("(?m)(\\S+)\\s+((\\S+)\\s+(\\S+))$").matcher(POEM);
		 
		 while(m.find()) {
			 System.out.print("(" + m.start() + ")");			 
			 for(int j = 0; j <= m.groupCount(); j++)
			 System.out.print("[" + m.group(j) + "]");
			 System.out.println("(" + m.end() + ")");	
		 }
	 }
	 
	 void Replacements(){
		 StringBuffer sbuf = new StringBuffer();
		 Pattern p = Pattern.compile("[aeiou]");
		 Matcher m = p.matcher(POEM);
		 
		 while(m.find())
			 m.appendReplacement(sbuf, m.group().toUpperCase());
			 // Put in the remainder of the text:
			 m.appendTail(sbuf);		// in this particular case appends "3ND 0F L1N3"
			 System.out.println(sbuf);   
			 
 
		 
	 }

}