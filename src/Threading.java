
public class Threading {
	public Threading() {
		MyExtendedThread thread1 = new MyExtendedThread();
		thread1.start();
		
		Thread thread2 = new Thread(new MyRunnableThread());
		thread2.start();
		
	}
	
}

class MyExtendedThread extends Thread{
	 public void run() {
		System.out.println("Important job running in MyThread");
	 	run("Pizdec!");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 	run("Pizdec2!");
	 	try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	 public void run(String s) {
		 System.out.println("String in run is " + s);
	 }
	}

class MyRunnableThread implements Runnable{
	
	public void run() {
		 System.out.println("Important job running in MyRunnable");
		 }
	
}
