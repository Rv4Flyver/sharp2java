import package1.Classes;

public class Accessibility {			// accessible only within default package because default package cannot be imported
	static long term = 100500; 			// default access - accessible for entire package only
	public Accessibility() {
		new Class3();
		this.term = 10;
	}
}

class Class1{							// default access - accessible for entire package only
	static long term = 100500; 			// default access - accessible for entire package only
}

class Class2{
	{
		System.out.println(Accessibility.term);
	}
}

class Class3 implements All{
	public void a(){}
	
	public void b(){}
}

interface IFace1{
	int time = 10;
	static final String ASDSA_STRING = "asdasd";
	void a();
}

interface IFace2{
	int time = 10;
	static final String ASDSA_STRING = "asdasd";
	void b();
}

interface All extends IFace1, IFace2{
	
}
