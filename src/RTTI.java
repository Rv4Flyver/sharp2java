public class RTTI {
	
	public RTTI() {
		try
		{
			System.out.println(Class.forName("Interfaces2").getInterfaces()[0]);
		}
		catch(ClassNotFoundException ex)
		{
			
		}
		
		// safe way to get Class, so does not demand try-catch block
		Class t = ObjectLifeCycle.class;
		System.out.println(t.getName());
		System.out.println(boolean.class.getTypeName() +" = " + Boolean.TYPE.getTypeName());
		//ObjectLifeCycle olc = new ObjectLifeCycle();
		
		SubClassRTTI scrtti = new SubClassRTTI();
		Class<SubClassRTTI> cls = SubClassRTTI.class;
		//Class<SuperClassRTTI> scls = (Class<SuperClassRTTI>)cls.getSuperclass();	
		Class<? super SubClassRTTI> scls = cls.getSuperclass();		
		System.out.println(scls.getSimpleName());
	}
	
	class SuperClassRTTI{
		
	}
	
	class SubClassRTTI extends SuperClassRTTI{
		
	}

}
